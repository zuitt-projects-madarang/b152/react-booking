import React from 'react'; 
import ReactDOM from 'react-dom';
import App from './App'

//import bootstrap css
import 'bootstrap/dist/css/bootstrap.min.css'

/*let element = <h1>Hello World!</h1>*/

/*
  The syntax used in Reactjs is JSX.

  JSX - Javascript + XML. It is an extension of Javascript that lets us create objects which will then be compiled and added as HTML elements.

  With JSX, we are able to create HTML elements using JS.
*/

/*console.log(element)*/
/*
  ReactDOM.render() - allows us to render/display our reactjs elements and display it in our html.

  ReactDOM.render(<reactElement>,<htmlElementSelectedById>)
*/
/*
  RectJS does not like rendering two adjacent elements. Instead, the adjacent elements must be weapped by a parent element or React Fragments.
  (<>...</>)

  If the parent of the elements is an actual HTML element, it will be added in the HTML/DOM.

  If the parent of the elements is a React Fragment, it will not be added in the HTML/DOM.
*/
/*let myName = (
  <>
    <h2>Adrian Madarang</h2>
    <h3>Full Stack Developer</h3>
  </>
  )*/
/*
  Embedding JS expressions in react elements with JSX

  Since JSX uses Javascript, it is much easier to embed JS expressions in our react elements. We simply add {} in our JSX created react elements.
*/

/*let name = "Jeffrey Lacdao"
let job = "Professional Driver"*/

/*let personDisplay1 = (
  <>
    <h1>{name}</h1>
    <h2>{job}</h2>
  </>
)*/

/*let num1 = 150
let num2 = 250*/

/*let numDisplay = (
  <>
    <h4>The sum of {num1} and {num2} is:</h4>
    <p>{num1+num2}</p>
  </>
)*/

/*let person = {
  name: "Stephen Strange",
  age: 45,
  job: "Sorcerer Supreme",
  income: 50000,
  expense: 30000,
  address: "New York City, New York"
}*/

/*let personDisplay2 = <p>Hi! My name is {person.name}. I am {person.age} years old. I live in {person.address}.</p>*/

/*let personDisplay3 = (
  <>
    <h1>{person.name}</h1>
    <h2>{person.job}</h2>
    <h3>Age: {person.age} years old</h3>
    <h3>Address: {person.address}</h3>
    <h4>Income: ${person.income}</h4>
    <h4>Expense: ${person.expense}</h4>
    <h4>Current balance: ${person.income-person.expense}</h4>
  </>
)*/

/*
  ReactJS components are parts of the page. They are independent UI parts of our app.

  Components are functions that return react elements.

  Components naming convention: PascalCase

  Components should start in capital letters.
*/

/*const SampleComp = () => {
  return(
      <h1>I am a react element returned by a function.</h1>
    )
}*/

//How are components called?
//Create a self-closiong tag <SampleComp /> to call your component.

/*
  In Reactjs, we normally render our components in an entry point or in a mother component called App. This is so we can group our components under a single entry point/main component.

  All other components/pages will be contained in our main component: <App />

  React.StrictMode is a built-in react component which is used to highlight potential problem in our code and in fact allows for more information about errors in our code.
*/

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
)
