import React from 'react'

/*
	Create a Context Object

	A context is a special React object which will allow us to store information within and pass it around our components.

	With this, we'll be able to create a global state to store our user details instead of having to save it in our localStorage.

	The context object is a different approach to passing information between components without the use of props and having to pass it from parent to child.
*/

const UserContext = React.createContext()

/*
	The Provider component is what allows other component to consume or use our context. Any component which is not wrapped by our Provider will not have access to the values provided in the context.
*/

export const UserProvider = UserContext.Provider

export default UserContext