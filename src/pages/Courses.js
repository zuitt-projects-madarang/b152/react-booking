//import useState,useEffect,useContext
import {useState,useEffect,useContext} from 'react'
import Banner from '../components/Banner'
import CourseCard from '../components/Course'
import AdminDashboard from '../components/AdminDashboard'

//import our user context
import UserContext from '../userContext'

export default function Courses(){

	const {user} = useContext(UserContext)
	//console.log(user)

	//console.log(coursesData)
	let courseBanner = {
		title: "Welcome to the Courses Page.",
		description: "View one of our courses below.",
		buttonText: "Register/Login to Enroll.",
		destination: "/register"
	}

	//create a state to save our course components for all active courses.
	const [coursesArray,setCoursesArray] = useState([])

	//create a useEffect which will retrieve our courses on initial render.
	useEffect(()=>{
		//fetch all ACTIVE courses
		//For requests that are GET method AND does not need to pass token, we don't need to add {options}
		fetch("http://localhost:4000/courses/getActiveCourses")
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			//create a new array of course components out of our array of documents (data):
			setCoursesArray(data.map(course => {
				return (
					<CourseCard key={course._id} courseProp={course} />
				)
			}))
		})
	},[])

	//console.log(coursesArray)

	/*
		Components are independent from one another. It may come from the same component file but when returned, each component is independent from one another.

		Remember, components are function. We run the same function multiple times.
	*/

	//let propCourse1 =coursesData[0]
	//let propCourse2 ="sample data 2"
	//let propCourse3 ="sample data 3"

	/*
		Display the first item in the coursesData array in our first CourseCard component.
	*/

	/*Create and array of CourseCard components by mapping out details from our coursesData array.*/
	/*let coursesComponents = coursesData.map(course => {
		//We essentially returned an array of CourseCard components into our new coursesComponents.
		//We are also able to pass the data of the current item being looped by map() by passing the course parameter in our anonymous function.
		//When we create an array of components, we have to add a unique identifier for each component. Pass a key prop with a unique identifier for each item in the array.
		return <CourseCard courseProp={course} key={course.id}/>
	})*/

	//console.log(coursesComponents)

	return(
		user.isAdmin
		? 
		<AdminDashboard />
		:
		<>
			<Banner bannerProp={courseBanner}/>
			{coursesArray}
		</>
	)
}