import {useState,useContext,useEffect} from 'react'
import {Form,Button} from 'react-bootstrap'

import Swal from 'sweetalert2'

import UserContext from '../userContext'

import {Navigate} from 'react-router-dom'

export default function Register(){

	const {user} = useContext(UserContext)
	//input states
	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [email,setEmail] = useState("")
	const [mobileNo,setMobileNo] = useState("")
	const [password,setPassword] = useState("")
	const [confirmPassword,setConfirmPassword] = useState("")

	const [isActive,setIsActive] = useState(false)

	useEffect(()=>{
		if(firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password !== "" && confirmPassword !== "" &&
		   mobileNo.length === 11 &&
		   password === confirmPassword){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	},[firstName,lastName,email,mobileNo,password,confirmPassword])

	/*
		Two Way Binding

		To be able to capture/save the input value from our input elements, we can bind the value of our element with our states. We cannot type into our inputs anymore because there is now value that is bound to it. We will add an onChange event to be able to update the state that was bound to our input.

		Two Way Binding is done so that we can assure that we can save the input into our states as we type into the input element. This is so we don't have to save it just before we submit.

		<Form.Control type="inputType" value={inputState} onChange{e => {setInputState(e.target.value)}}/>

		e = event, all event listeners can pass the event object. The event object contains all the details about the event, what happened? where did it happen? what is the current value of the element where happened?

		e.target = target is the element WHERE the event happened.

		e.target.value = value is a property of target. It is the current value of the element WHERE the event happened.
	*/

	function registerUser(e){
		//prevent submit event's default behavior
		e.preventDefault()

		/*console.log(firstName)
		console.log(lastName)
		console.log(email)
		console.log(mobileNo)
		console.log(password)
		console.log(confirmPassword)*/

		//fetch() is a JS method which allows us to pass/create a request to an api.
		//syntax: fetch("<requestURL>",{options})
		fetch('http://localhost:4000/users/',{
			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			//data is the response of the api/server after it's been processed as JS object through our res.json() method
			console.log(data)
			//data will only contain an email property if we can properly save our user.
			if(data.email){
				Swal.fire({
					icon: "success",
					title: "Registration Successful",
					text: "Thank you for registering!"
				})

				//redirect the user
				window.location.href = "/login"
			} else {
				Swal.fire({
					icon: "error",
					title: "Registration Failed",
					text: "Something went wrong."
				})
			}
		})
	}

	return (
		user.id
		?
		<Navigate to="/courses" replace={true} />
		:
		<>
			<h1 className="my-5 text-center">Register</h1>
			<Form onSubmit={e => registerUser(e)}>
				<Form.Group>
					<Form.Label>First Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter First Name" required value={firstName} onChange={e => {setFirstName(e.target.value)}}/>
				</Form.Group>	
				<Form.Group>
					<Form.Label>Last Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter Last Name" required value={lastName} onChange={e => { setLastName(e.target.value)}}/>
				</Form.Group>	
				<Form.Group>
					<Form.Label>Email:</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" required value={email} onChange={e => {setEmail(e.target.value)}}/>
				</Form.Group>	
				<Form.Group>
					<Form.Label>Mobile No:</Form.Label>
					<Form.Control type="number" placeholder="Enter 11 Digit No." required value={mobileNo} onChange={e => {setMobileNo(e.target.value)}}/>
				</Form.Group>	
				<Form.Group>
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" required value={password} onChange={e => {setPassword(e.target.value)}}/>
				</Form.Group>	
				<Form.Group>
					<Form.Label>Confirm Password:</Form.Label>
					<Form.Control type="password" placeholder="Confirm Password" required value={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}}/>
				</Form.Group>
				{
					isActive
					? <Button variant="primary" type="submit" className="my-5">Submit</Button>
					: <Button variant="primary" disabled className="my-5">Submit</Button>
				}
			</Form>
		</>
	)
}