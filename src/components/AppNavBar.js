//import useContext() hook
import {useContext} from 'react'

//import react-bootstrap components:
import {Nav,Navbar,Container} from 'react-bootstrap' 

//import user context:
import UserContext from '../userContext'

export default function AppNavBar(){

	/*
		useContext() hook will allow us to unwrap the value provided by our UserProvider from our UserContext. useContext() will return an object after unwrapping our context.
	*/

	//console.log(useContext(UserContext))
	//destructure our UserContext
	const {user} = useContext(UserContext)
	//console.log(user)

	return(
		<Navbar bg="primary" expand="lg">
			<Container fluid>
				<Navbar.Brand href="/">B152 Booking</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto">
						<Nav.Link href="/">Home</Nav.Link>
						<Nav.Link href="/courses">Courses</Nav.Link>
						{
							user.id 
							? 
								user.isAdmin
								?
								<>
									<Nav.Link href="/addCourse">Add Course</Nav.Link>
									<Nav.Link href="/logout">Logout</Nav.Link>
								</>
								:
								<Nav.Link href="/logout">Logout</Nav.Link>
							: 
							<>
								<Nav.Link href="/register">Register</Nav.Link>
								<Nav.Link href="/login">Login</Nav.Link>
							</>
						}
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}