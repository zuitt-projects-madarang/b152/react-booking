import {Row,Col,Card} from 'react-bootstrap'

export default function Highlights({highlightsProp}){
	//console.log(highlightsProp)
	return(
		<Row className="my-3">
			<Col xs={12} md={4}>
				<Card className="p-3 cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>Learn From Home</h2>
						</Card.Title>
						<Card.Text>
							Proident qui esse eu sed elit do ut est reprehenderit cupidatat exercitation esse qui incididunt officia pariatur in.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="p-3 cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>Study Now, Pay Later</h2>
						</Card.Title>
						<Card.Text>
							Dolore dolor esse ad veniam sit consequat proident in est dolor sunt dolor labore ut dolor sed occaecat dolor commodo dolore exercitation commodo reprehenderit in in excepteur quis esse minim eu adipisicing laboris ut.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="p-3 cardHighlight">
					<Card.Body>
						<Card.Title>
							<h2>Be Part of Our Community</h2>
						</Card.Title>
						<Card.Text>
							Proident qui nostrud dolor aliquip elit esse laborum id sed. Ad ut proident enim duis nulla cupidatat dolore cupidatat est officia proident ut est enim velit ullamco.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}