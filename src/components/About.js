import {Row, Col} from 'react-bootstrap'

export default function About(){
	return(
		<Row>
			<Col className="p-5 bg-warning text-success">
				<h1 className="my-5">About Me</h1>
				<h2 className="mt-3">Adrian Joseph Chua Madarang</h2>
				<h3>Full Stack Web Developer</h3>
				<p className="mb-4">I am a Full Stack Web Developer from Zuitt Coding Bootcamp. I am a licensed engineer but have decided to shift careers.</p>
				<h4>Contacts</h4>
				<ul>
					<li>Email: amadarangbiz@gmail.com</li>
					<li>Mobile No.: 09474628816</li>
					<li>Address: La Trinidad, Benguet</li>
				</ul>
			</Col>
		</Row>
	)
}