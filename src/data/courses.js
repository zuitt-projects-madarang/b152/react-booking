//mockdata for courses
let coursesData = [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Amet veniam sed sit amet tempor est dolor ut ea laborum sunt reprehenderit fugiat aliqua.",
		price: 25000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description: "Tempor deserunt consectetur ut qui tempor ut nisi laboris ullamco ad duis nisi non eu laboris ea aliqua velit laboris commodo cupidatat laboris in fugiat deserunt duis quis ullamco officia.",
		price: 35000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Quis cupidatat ut in irure ad magna occaecat veniam culpa duis consequat do in.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc004",
		name: "How to get rich fast",
		description: "Duis fugiat id consequat nostrud anim irure in culpa aliquip sunt in dolor ullamco.",
		price: 1000000,
		onOffer: true
	}
]

export default coursesData